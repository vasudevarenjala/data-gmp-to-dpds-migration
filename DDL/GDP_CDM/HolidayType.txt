CREATE TABLE HolidayType(
	SF_BatchID string NOT NULL,
	SF_LegalEntity string NOT NULL,
	HolidayTypeSk bigint NOT NULL,
	MarketSk bigint NOT NULL,
	HolidayTypeCode nvarchar(20) NULL,
	HolidayTypeName nvarchar(128) NULL,
	ModifiedBy nvarchar(250) NULL,
	ModifiedDate datetime NULL,
	BatchId int NULL
);