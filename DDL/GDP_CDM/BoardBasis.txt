create table boardbasis(
	sf_batchid string,  
	sf_legalentity string,
	boardbasissk bigint not null,
	marketsk bigint not null,
	boardbasiscode nvarchar(20) null,
	boardbasisname nvarchar(128) null,
	modifiedby nvarchar(250) null,
	modifieddate datetime null,
	batchid int null
);