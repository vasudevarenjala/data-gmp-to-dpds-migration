CREATE TABLE DestinationSummary(
	SF_BatchID string NOT NULL,
	SF_LegalEntity string NOT NULL,
	MarketCode nvarchar(20) NULL,
	DestinationSummaryID bigint IDENTITY(1,1) NOT NULL,
	CustomerMk bigint NOT NULL,
	MarketSk bigint NOT NULL,
	BrandSk bigint NOT NULL,
	ProductMk bigint NULL,
	BoardBasisSk bigint NULL,
	DepartureAirportSk bigint NULL,
	CountryMk bigint NULL,
	DestinationMk bigint NULL,
	ResortMk bigint NULL,
	HotelMk bigint NULL,
	BrandCode nvarchar(20) NULL,
	CustomerLocalId nvarchar(255) NULL,
	ProductCode nvarchar(20) NULL,
	BoardBasisCode nvarchar(20) NULL,
	DepartureAirportCode nvarchar(20) NULL,
	CountryCode nvarchar(20) NULL,
	DestinationCode nvarchar(20) NULL,
	ResortCode nvarchar(20) NULL,
	HotelCode nvarchar(20) NULL,
	EmailAddressSK bigint NULL,
	Rank1Day int NULL,
	Rank3Days int NULL,
	Rank7Days int NULL,
	Rank14Days int NULL,
	Rank15Days int NULL,
	Rank28Days int NULL,
	Rank56Days int NULL,
	Duration int NULL,
	StarRating int NULL,
	NumberOfAdults int NULL,
	NumberOfChildren int NULL,
	NumberOfInfants int NULL,
	NumberOfPassengers int NULL,
	PartySize int NULL,
	Budget decimal(18, 2) NULL,
	ModifiedBy nvarchar(250) NULL,
	ModifiedDate datetime NULL,
	BatchId int NULL,
	HotelsViewedCount7Days int NULL,
	DepartureDate date NULL,
	HolidayTypeSk bigint NULL,
	HolidayTypeCode nvarchar(20) NULL
);