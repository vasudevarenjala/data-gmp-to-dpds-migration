CREATE TABLE marketlookup (
	sf_batchid STRING,
	sf_legalentity STRING,   
	marketlookupsk INT,
	sourcesystem STRING,
	sourcemarketcode STRING,
	datalakemarketcode STRING,
	enteredby STRING,
	entereddate TIMESTAMP,
	createdby STRING,
	createddate TIMESTAMP,
	modifiedby STRING,
	modifieddate TIMESTAMP,
	batchid INT
);