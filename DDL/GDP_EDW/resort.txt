CREATE TABLE resort (   
	sf_batchid STRING,  
	sf_legalentity STRING,                                                     
	rowkey STRING,                                                                                   
	resortsk BIGINT,                                                                                 
	resortmk BIGINT,                                                                                 
	uniqueresortmk BIGINT,                                                                           
	destinationmk BIGINT,                                                                            
	resortcode STRING,                                                                               
	resortname STRING,                                                                               
	sourcelookupvalue STRING,                                                                        
	active INT,                                                                                      
	fromdate TIMESTAMP,                                                                              
	todate TIMESTAMP,                                                                                
	createddate TIMESTAMP,                                                                           
	createdby STRING,                                                                                
	modifieddate TIMESTAMP,                                                                          
	modifiedby STRING,                                                                               
	batchid INT
);                                                                                                  
