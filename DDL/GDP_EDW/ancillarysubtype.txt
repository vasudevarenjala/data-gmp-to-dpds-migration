CREATE TABLE ancillarysubtype (
	sf_batchid STRING,
	sf_legalentity STRING,   
	rowkey STRING,
	ancillarysubtypesk INT,
	ancillarytypemk INT,
	ancillarysubtypemk INT,
	ancillarysubtypecode STRING,
	ancillarysubtypename STRING,
	uniqueancillarysubtypemk INT,
	createddate TIMESTAMP,
	createdby STRING,
	modifieddate TIMESTAMP,
	modifiedby STRING,
	batchid INT
);

