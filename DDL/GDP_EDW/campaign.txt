CREATE TABLE campaign ( 
	sf_batchid STRING,
	sf_legalentity STRING,                                                                                                             
	rowkey STRING,                                                                                                                                             
	campaignsk BIGINT,                                                                                                                                         
	campaignid STRING,                                                                                                                                         
	campaigncode STRING,                                                                                                                                       
	campaignname STRING,                                                                                                                                       
	initiative STRING,                                                                                                                                         
	createdby STRING,                                                                                                                                          
	createddate TIMESTAMP,                                                                                                                                     
	modifiedby STRING,                                                                                                                                         
	modifieddate TIMESTAMP,                                                                                                                                    
	batchid INT                                                                                                                                                
 ); 