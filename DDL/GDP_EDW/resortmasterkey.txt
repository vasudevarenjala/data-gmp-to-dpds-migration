CREATE TABLE resortmasterkey (                                              
	sf_batchid STRING,  
	sf_legalentity STRING,                                                     
	rowkey STRING,                                                                                     
	resortmk BIGINT,                                                                                   
	businesskey STRING,                                                                                
	businesskeytype STRING,                                                                            
	sourcesystem STRING,                                                                               
	createddate TIMESTAMP,                                                                             
	createdby STRING,                                                                                  
	modifieddate TIMESTAMP,                                                                            
	modifiedby STRING,                                                                                 
	batchid INT                                                                                        
 ); 