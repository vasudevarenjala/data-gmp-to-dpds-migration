CREATE TABLE customermasterkey (   
	sf_batchid STRING,  
	sf_legalentity STRING,                                                                    
	rowkey STRING,                                                                                                             
	customermk BIGINT,                                                                                                         
	businesskey STRING,                                                                                                        
	businesskeytype STRING,                                                                                                    
	sourcesystem STRING,                                                                                                       
	sourcelookupvalue STRING,                                                                                                  
	createddate TIMESTAMP,                                                                                                     
	createdby STRING,                                                                                                          
	modifieddate TIMESTAMP,                                                                                                    
	modifiedby STRING,                                                                                                         
	batchid INT,                                                                                                               
	customerid INT,                                                                                                            
	emailid STRING,                                                                                                            
	sourcesystemsk BIGINT,                                                                                                     
	issourcerecordflag STRING,
	customernumber STRING
);                                                                                               
                                                                                                                            
