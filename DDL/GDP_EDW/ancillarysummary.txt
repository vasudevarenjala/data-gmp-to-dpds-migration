CREATE TABLE ancillarysummary (
	sf_batchid STRING,
	sf_legalentity STRING,   
	marketsk INT,
	brandsk INT,
	customermk INT,
	addresssk INT,
	ancillarymk INT,
	journeymk INT,
	bookingmk INT,
	marketcode STRING,
	brandcode STRING,
	customerlocalid STRING,
	bookingnumber STRING,
	ancillarycode STRING,
	ancillarysubtypecode STRING,
	ancillarytypecode STRING,
	emailaddresssk BIGINT,
	ancillarystartdate STRING,
	ancillaryenddate STRING,
	ancillarydate STRING,
	quantity INT,
	totalrevenue DOUBLE,
	custom1 STRING,
	custom2 STRING,
	custom3 STRING,
	custom4 STRING,
	custom5 STRING,
	custom6 STRING,
	custom7 STRING,
	custom8 STRING,
	custom9 STRING,
	custom10 STRING,
	modifieddate TIMESTAMP,
	modifiedby STRING,
	batchid INT
);
