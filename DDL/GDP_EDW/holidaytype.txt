CREATE TABLE holidaytype ( 
	sf_batchid STRING,
	sf_legalentity STRING,                                      
	holidaytypesk BIGINT,                                                                  
	holidaytypecode STRING,                                                                
	holidaytypename STRING,                                                                
	enteredby STRING,                                                                      
	entereddate TIMESTAMP,                                                                 
	createdby STRING,                                                                      
	createddate TIMESTAMP,                                                                 
	modifiedby STRING,                                                                     
	modifieddate TIMESTAMP,                                                                
	batchid INT                                                                            
 ); 