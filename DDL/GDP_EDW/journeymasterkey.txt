CREATE TABLE journeymasterkey ( 
	sf_batchid STRING,
	sf_legalentity STRING,                                                                                                           
	key STRING,                                                                                                                                                      
	journeymk BIGINT,                                                                                                                                                
	businesskey STRING,                                                                                                                                              
	businesskeytype STRING,                                                                                                                                          
	sourcesystem STRING,                                                                                                                                             
	createddate TIMESTAMP,                                                                                                                                           
	createdby STRING,                                                                                                                                                
	modifieddate TIMESTAMP,                                                                                                                                          
	modifiedby STRING,                                                                                                                                               
	batchid INT                                                                                                                                                      
 ); 