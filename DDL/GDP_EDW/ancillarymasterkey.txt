 CREATE TABLE ancillarymasterkey (
	sf_batchid STRING,
	sf_legalentity STRING,   
	ancillarymk INT,
	ancillaryid STRING,
	sourcesystem STRING,
	createdby STRING,
	createddate TIMESTAMP,
	modifiedby STRING,
	modifieddate TIMESTAMP,
	batchid INT,
	rowkey STRING,
	businesskey STRING,
	businesskeytype STRING
);